export interface Joke {
  id: number;
  type: string;
  setup: string;
  punchline: string;
}

import { UnleashClient } from 'unleash-proxy-client';

const unleash = new UnleashClient({
    url: 'https://gitlab.com/api/v4/feature_flags/unleash/45535566',
    clientKey: 'ufxP82FgRqoBPpxvdQUs',
    appName: 'nextjs-vercel',
});

// Start the background polling
unleash.start();

unleash.on('ready', () => {
  if (unleash.isEnabled('jokes')) {
      console.log('proxy.demo is enabled');
  } else {
      console.log('proxy.demo is disabled');
  }
});

export default function HomePage({ jokes }: { jokes: Joke[] }) {

  const show_jokes = unleash.isEnabled("jokes")

  console.log(show_jokes);

  let jokes_list;

  if(show_jokes){
    jokes_list = <ul>{jokes.map((joke) => (<li className="ml-5 mt-5" key={joke.id}><span className="text-xl">{joke.setup}</span><br />{joke.punchline}</li>))}</ul>
  } else {
    jokes_list = <p>No jokes</p>
  }

  return (
    <div className="flex flex-col">
      <h1 className="text-3xl font-bold">Jokes</h1>
      {jokes_list}
    </div>
  );
}
